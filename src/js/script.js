
function changeColor() {
  coolColors = ["#001f3f", "#0074D9", "#7FDBFF", "#39CCCC", "#3D9970", "#2ECC40", "#01FF70", "#FFDC00"];
  coolColor = coolColors[Math.floor(Math.random() * coolColors.length)];
  document.getElementById("mainHeadline").style.color = coolColor;
}

function noArrowAnimation() {

  var animationText = document.getElementById("deactiviateArrowAnimation");
  var arrow = document.getElementById("arrowLeft");

  if(animationText.innerHTML == "Animation deaktivieren") {
    arrow.classList.remove("animate");
    animationText.innerHTML = "Animation aktivieren";
    return;
  }

  if(animationText.innerHTML == "Animation aktivieren") {
    arrow.classList.add("animate");
    animationText.innerHTML = "Animation deaktivieren";
    return;
  }
}

function loadStuff() {
  document.getElementById("section").innerHTML='<object type="text/html" data="foo.html"></object>';
}

document.getElementById("mainHeadline").addEventListener("click", changeColor);


document.getElementById("deactiviateArrowAnimation").addEventListener("click", noArrowAnimation);


// Scroll To Top Button
const btnScrollToTop = document.querySelector("#btnScrollToTop");
btnScrollToTop.addEventListener("click", function() {
  window.scrollTo({
    top: 0,
    left: 0, 
    behavior: "smooth"
  });
});




