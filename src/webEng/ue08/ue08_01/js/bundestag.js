import { LitElement, html, svg, css } from 'https://unpkg.com/lit-element/lit-element.js?module';

class VerteilungBundestag extends LitElement {
  static get properties() {
    return {
      fetchDataDone: {type: Boolean},
      partyArray: {type: Array },
      spaceBetweenBars: { tpype: Number },
      spaceTextXposition: { tpype: Number }
    };
  }

  static get styles() {
    return css`
      .animateBar {
        animation: scale 5s;
      }

      .animateText {
        animation: showText 6s;
      }

      @keyframes scale {
        from {
          transform: scale(0);
        }

        to {
          transform: scale(1);
        }
      }

      @keyframes showText {
        0%,88% {
          opacity: 0;
        }

        90% {
          opacity: 1;
        }
      }
    `;
  }

  constructor() {
    super();
  
    this.partyArray = [];
    this.spaceBetweenBars = 0;
    this.spaceTextXposition = 0;
    this.fetchDataDone = false;
    
    // wenn man keinen lokalen Server
    // parseJSONviaText verwenden anstatt parseJSONviaFetch
    //this.parseJSONviaText();
    
    this.parseJSONviaFetch();
  }

  parseJSONviaText() {

    var jsonData = '[{"party":"cdu","seats":246,"partyColor":"#000000"},{"party":"spd","seats":152,"partyColor":"#e3010f"},{"party":"afd","seats":91,"partyColor":"#009de0"},{"party":"fdp","seats":69,"partyColor":"#feed01"},{"party":"Die Linke","seats":69,"partyColor":"#e50097"},{"party":"Die Grünen","seats":67,"partyColor":"#65a22d"},{"party":"others","seats":4,"partyColor":"#cccccc"}]';
    var data = JSON.parse(jsonData);

    for(var i = 0; i < data.length; i++) {
      this.partyArray.push(data[i]);      
    }
  }

  parseJSONviaFetch() {

    // first parse external json file
    const fetchURL = "data.json";
    
    
    let request = async() => {
      let response = await fetch(fetchURL);
      let data = await response.json();

      for(var i = 0; i < data.length; i++) {
        this.partyArray.push(data[i]);      
      }

      this.fetchDataDone = true;
    }

    request();
    
  }


  calculateRectangleHeight(seats) {
    var maxRecHeight = 500;
    return maxRecHeight - seats;
  }

  calculateTextYheight(seats) {
    var rtHeight = this.calculateRectangleHeight(seats);
    return rtHeight - 5;
  }

  getTextXposition() {
    var oldValue = this.spaceTextXposition;
    this.spaceTextXposition = this.spaceTextXposition + 50;
    return oldValue;
  }

  getSpaceBetweenBars() {
    var oldValue = this.spaceBetweenBars;
    this.spaceBetweenBars = this.spaceBetweenBars + 50;
    return oldValue;
  }


  render() {

    while(!this.fetchDataDone) {
      return html`<p>loading data...</p>`;
    }

    return html`    
      <svg viewbox="0 0 500 500" width=500 height=500>
        ${this.partyArray.map((item) =>
          svg`<rect class="animateBar" x=${this.getSpaceBetweenBars()} y=${this.calculateRectangleHeight(item.seats)} width=30 height=500 fill=${item.partyColor} >
              <!-- Animate funktioniert hier drin nicht -->
              </rect>
              <text class="animateText" x=${this.getTextXposition()} y=${this.calculateTextYheight(item.seats)} fill="black">${item.seats}</text>
          `)}
          
        <line x1=0 y1=500 x2=500 y2=500 stroke="black" stroke-width=2></line>
      </svg>
    `;
  }
}

customElements.define('verteilung-bundestag', VerteilungBundestag);