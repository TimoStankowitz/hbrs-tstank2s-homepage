const express = require('express');
const mongodb = require('mongodb');

const uri = "mongodb+srv://user1:Fcm7dYuZbltrEmmE@cluster0.8seuq.mongodb.net/vue_express?retryWrites=true&w=majority";

const router = express.Router();

// get Posts
router.get('/', async (req, res) => {
  const posts = await loadPostCollection();
  res.send(await posts.find({}).toArray());
});

// add Post
router.post('/', async (req, res) => {
  const posts = await loadPostCollection();
  await posts.insertOne({
    text: req.body.text,
    createdAt: new Date()
  });

  res.status(201).send();
});




async function loadPostCollection() {
  const client = await mongodb.MongoClient.connect(
    'mongodb+srv://user1:Fcm7dYuZbltrEmmE@cluster0.8seuq.mongodb.net/vue_express?retryWrites=true&w=majority', {
      useNewUrlParser: true
    }
  );  
  return client.db('vue_express').collection('posts');
}


module.exports = router;

