function createArrayWithRandomNumbers(numberOfEntities) {
  var numbers = [];

  for(i = 0; i < numberOfEntities; i++) {
    numbers.push(Math.round(Math.random() * 100000));
  }

  return numbers;
}

var numbers = createArrayWithRandomNumbers(10000);
var sortedNumbers = numbers.sort((a,b) => a - b)

document.querySelector("#displayArray").innerHTML = sortedNumbers;
