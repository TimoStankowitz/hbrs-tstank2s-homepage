
async function getText(path, displayInHTML) {

  try {
    var request = await fetch(path);
    var text = await request.text();
    document.querySelector(displayInHTML).innerHTML = text
  } catch (error) {
    document.querySelector("#errorHeadline").style.display = "block";
    document.querySelector("#errorMessage").innerHTML = error;
  }
}

getText('files/a.txt', '#text1');
getText('files/b.txt', '#text2');

