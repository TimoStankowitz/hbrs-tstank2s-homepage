
var menuJSON_data = null;

fetch("./menues.json")
  .then(res => res.json())
  .then(json => useJSON_data(json))
  .catch(function() {
    alert("error loading json file");
  });

function useJSON_data(data) {
  menuJSON_data = data;
  createHeader(data);
}
  

function createHeader(data) { 
  var allHeaderElements = Object.keys(data);

  var menu = document.querySelector("#headerList");

  allHeaderElements.forEach(function(headerElement) {
    var li = document.createElement("li");
    var anchor = document.createElement("a");

    anchor.innerText = headerElement;
    anchor.setAttribute('data-id', headerElement);
    addClickEventHeaderButton(anchor);

    li.appendChild(anchor);
    menu.appendChild(li);

  });

}


function addClickEventHeaderButton(element) {
  element.addEventListener("click", function() {
    loadNavElements(element);
  });
}

function loadNavElements(element) {
  var headerElementName = element.getAttribute('data-id');

    
  var navList = document.querySelector("#navList");

  var elements = menuJSON_data[headerElementName];

  var allNavElements = Object.keys(elements);

  // first clean the navList
  while(navList.firstChild) {
    navList.removeChild(navList.firstChild);
  }

  // load sidebar
  allNavElements.forEach(function(navElement) {
    var li = document.createElement("li");
    var anchor = document.createElement("a");

    anchor.innerText = navElement;
    anchor.className = "navButton";
    anchor.setAttribute('data-id', navElement);
    addClickEventNavButton(anchor, headerElementName);

    li.appendChild(anchor);
    navList.appendChild(li);
  });

}

function addClickEventNavButton(navElement, headerElementName) {
  navElement.addEventListener("click", function() {
    loadSectionContent(navElement, headerElementName);
  });
}

function loadSectionContent(navElement, headerElementName) {
  var dataNavElement = navElement.getAttribute('data-id');

  // remove old content
  var section = document.querySelector("section");
  section.removeChild(section.firstChild);

  //console.log("Das header element: " + headerElementName);
  //console.log("NavElement: " + dataNavElement);

  var text = menuJSON_data[headerElementName][dataNavElement];

  section.innerHTML = text.content;
}



