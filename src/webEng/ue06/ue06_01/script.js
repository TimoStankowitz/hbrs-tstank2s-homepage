function getText(path, displayInHTML) {
  fetch(path)
  .then(function(response) { // this is the promise. Ich versucht ...
    return response.text(); // Wichtig: Ich muss hier was zurück geben.
  })
  .then(function(data) {    // wenn das funktioniert hat, dann wird folgendes ausgeführt
    document.querySelector(displayInHTML).innerHTML = data
  }) 
  .catch(function(error) {  // Wenn das Promise failed, dann kommt der Fehler
    document.querySelector("#errorHeadline").style.display = "block";
    document.querySelector("#errorMessage").innerHTML = error;
  });
}

getText('files/a.txt', '#text1');
getText('files/b.txt', '#text2');
