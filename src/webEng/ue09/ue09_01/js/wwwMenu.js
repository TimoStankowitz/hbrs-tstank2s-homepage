import { LitElement, html, css } from 'https://unpkg.com/lit-element/lit-element.js?module';
import { routerMixin } from 'https://unpkg.com/lit-element-router/lit-element-router.js?module';

import "./app-link.js";

class WorldWideWebNavigator extends routerMixin(LitElement) {
  static get properties() {
    return {
      route: { type: String },
      params: { type: Object },
      query: { type: Object },
      data: { type: Object },
      menuData: { type: Object },
      mainTopic: { type: Array },
      subTopic: { type: Array },
      fetchDataDone: {type: Boolean},
    };
  }

  static get styles() {
    return css`
      :root {
        --buttons-text-color: black;
        --buttons-bg-color: lightsteelblue;
        --buttons-border-color: salmon;
        --buttons-hover-bg-color: lightgoldenrodyellow;
        --buttons-hover-border-color: lightskyblue;
      }

      .container {
        margin: 1rem;
        padding: 0;
        display: flex;
        flex-flow: row wrap;
        justify-content: space-around;
        font-family: sans-serif;
      }

      header {
        background: #ffbf8e;
        color: #fff;
        flex: 1 100%;
        border-radius: 1rem 1rem 0 0;
      }

      header h1 {
        text-align: center;
      }
  
      header ul {
        list-style: none;
      }
  
      header ul li {
        display: inline-block;
        background: var(--buttons-bg-color);
        margin: 0 2rem;
        border-radius: 16px;
        border: 2px solid var(--buttons-border-color);
        transition: 0.4s;
      }
  
      header ul li:hover {
        background: var(--buttons-hover-bg-color);
        border: 2px solid var(--buttons-hover-border-color);
      }
  
      nav {
        flex: 1;
        background: lightpink;
        height: 80vh;
        padding: 1rem;
      }

      nav ul {
        list-style: none;
      }
  
      nav ul li {
        background: var(--buttons-bg-color);
        margin: 2rem 0;
        text-align: center;
        border: 2px solid var(--buttons-border-color);
        border-radius: 20px;
        transition: 0.4s;
      }
  
      nav ul li:hover {
        background: var(--buttons-hover-bg-color);
        border: 2px solid var(--buttons-hover-border-color);
      }
  
      section {
        flex: 2;
        background: lightskyblue;
        padding: 1rem;
      }
  
  
      aside {
        flex: 1;
        background: lightpink;
        padding: 1rem;
      }
  
      footer {
        flex: 1 100%;
        background: #c3c3c3;
        border-radius: 0 0 1rem 1rem;
      }
  
      footer p {
        text-align: center;
      }
    `;
  }

  static get routes() {
    return [
      {
        name: "mainTopic",
        pattern: ":mainTopic"
      },
      {
        name: "mainAndSubTopic",
        pattern: ":mainTopic/:subTopic"
      }
    ]
  }

  constructor() {
    super();
    this.route = "";
    this.params = {};
    this.query = {};
    this.data = {};

    // loading data
    this.fetchDataDone = false;
    this.loadData();
  }

  // übernommen aus der Demo
  router(route, params, query, data) {
    this.route = route;
    this.params = params;
    this.query = query;
    this.data = data;
    console.log(route, params, query, data);
  }

  loadData() {
    const fetchURL = "menues.json";

    let requestData = async() => {
      let response = await fetch(fetchURL);
      let data = await response.json();

      this.menuData = data;
      this.mainTopic = Object.keys(this.menuData);

      this.fetchDataDone = true;
    }

    requestData();
  }

  loadNav(mainTopic) {
    if(mainTopic != undefined) {
      var elements = this.menuData[mainTopic];

      var allNavElements = Object.keys(elements);
      this.subTopic = allNavElements;

      return html`
        ${this.subTopic.map(i => html`<li><app-link href="/${mainTopic}/${i}">${i}</li>`)}
      `;
    }
  }

  loadSection(mainTopic, subTopic) {
    if(mainTopic != undefined && subTopic != undefined) {
      var element = this.menuData[mainTopic][subTopic];
      return html`${element.content}`
    }
  }

  loadAside(mainTopic, subTopic) {
    if(mainTopic != undefined && subTopic != undefined) {
      var element = this.menuData[mainTopic][subTopic];
      return html`${element.source}`
    }
  }

  

  render() {

    while(!this.fetchDataDone) {
      return html`<p>loading data...</p>`;
    }

    return html`
    <div class="container">
      <header>
        <h1>WWW Navigator</h1>
        <ul>
          ${this.mainTopic.map(i => html`<li><app-link href="/${i}">${i}</app-link></li>`)}
        </ul>
      </header>

      <nav>
        <ul>
          ${this.loadNav(this.params.mainTopic)}
        </ul>
      </nav>

      <section>${this.loadSection(this.params.mainTopic, this.params.subTopic)}</section>

      <aside>
        <h2>Quelle</h2>
        ${this.loadAside(this.params.mainTopic, this.params.subTopic)}
      </aside>

      <footer>
        <p>Diese Seite wurde von Timo Stankowitz erstellt</p>
      </footer>
      </div>
      `;
  }

  
}
customElements.define('www-navigator', WorldWideWebNavigator);