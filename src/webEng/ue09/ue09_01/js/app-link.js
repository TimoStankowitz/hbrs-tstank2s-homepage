import { LitElement, html, css} from 'https://unpkg.com/lit-element/lit-element.js?module';
import { linkMixin } from 'https://unpkg.com/lit-element-router/lit-element-router.js?module';


export class Link extends linkMixin(LitElement) {
 
  static get properties() {
    return {
      href: { type: String }
    };
  }

  static get styles() {
    return css`
      a {
        text-decoration: none;
        display: inline-block;
        padding: 0.7rem 3rem;
        color: green;
        font-weight: bold;
        cursor: pointer;
      }
      `;
  }

  constructor() {
    super();
    this.href = "";
  }

  render() {
    return html`
      <a href="${this.href}" @click="${this.linkClick}">
        <slot></slot>
      </a>
      `;
  }

  linkClick(event) {
    event.preventDefault();
    this.navigate(this.href);
  }

  
}
customElements.define('app-link', Link);