/*
 * Diese Aufgaben wurden in Zusammenarbeit mit Tim van der Meulen bearbeitet (tmeule2s) 
 */

// Aufgabe 1 - identity_function
function identity_function(text) {
	return function() {
		return text;
  };
}

console.log(identity_function("Hallo")());


// Aufgabe 2 - addf
function addf(x) {
	return function(y) {
		return x+y;
  };
}

console.log(addf(3)(5));


// Aufgabe 3 - applyf
function add(x, y) {
	return x + y;
}

function mul(x, y) {
	return x * y;
}


function applyf(func){
	return function(x) { 
  	return function(y) {
    	return func(x,y);
    }
  }
}

console.log(applyf(add)(5)(2));
console.log(applyf(mul)(5)(6));

// Aufgabe 4

function add(x, y) {
	return x + y;
}

function mul(x, y) {
	return x * y;
}

// curry

function curry(binFunction, x) {
	return function(y){
  	return binFunction(x,y);
  }
}

var add3 = curry(add, 3);

console.log(add3(4));
console.log(add3(5));

console.log(curry(mul, 5)(6));


// Aufgabe 5

function add(x, y) {
	return x + y;
}

function mul(x, y) {
	return x * y;
}

// curry
function curry(binFunction, x) {
	return function(y){
  	return binFunction(x,y);
  }
}

function incAdd(x){
	return curry(add,x)(1);
}


function incMul(x) {
	return curry(mul, x)(2);
}



console.log(incAdd(7));

console.log(incMul(3));

// Aufgabe 6
function add(x, y) {
	return x + y;
}

function mul(x, y) {
	return x * y;
}

function methodize(func) {
	return function(x){
  	return func(this,x);
  }
}

Number.prototype.add = methodize(add);
console.log((3).add(4));

Number.prototype.mul = methodize(mul);
console.log((2).mul(2));

// Aufgabe 7
function add(x, y) {
	return x + y;
}

function mul(x, y) {
	return x * y;
}

function methodize(func) {
	return function(x){
  	return func(this,x);
  }
}

function demethodize(func) {
	return function(x,y){
  	return func.call(x,y);
  }
}

Number.prototype.add = methodize(add);

ret = demethodize(Number.prototype.add)(5,6);
console.log(ret);


// Aufgabe 8
function add(x, y) {
	return x + y;
}

function mul(x, y) {
	return x * y;
}

function twice(binFunct) {
	return function(x) {
  	return binFunct(x,x);
  }
}

var double = twice(add);
var square = twice(mul);


console.log(double(11));
console.log(square(11));

console.log(square(11) === 121);


// Aufgabe 9 
function add(x, y) {
	return x + y;
}

function mul(x, y) {
	return x * y;
}

function twice(binFunct) {
	return function(x) {
  	return binFunct(x,x);
  }
}

function composeu(func1, func2){
	return function(x){
  	return func2(func1(x));
  }
}

var double = twice(add);
var square = twice(mul);


console.log(composeu(double, square)(3));


// Aufgabe 10

function add(x, y) {
	return x + y;
}

function mul(x, y) {
	return x * y;
}

function composeb(func1, func2){
	return function(x, y, z){
  	return func2(func1(x, y), z);
  }
}


console.log(composeb(add, mul)(2, 3, 5));


// Aufgabe 11
function add(x, y) {
	return x + y;
}

function mul(x, y) {
	return x * y;
}

function once(func1){
	var executed = false;
  
	return function(x, y){
    if(!executed){
    	executed = true;
  		return func1(x, y);
    } else {
    	throw "no";
    }
  }
}

add_once = once(add);

mul_once = once(mul);

// add
console.log(add_once(3,4));

try{
	console.log(add_once(3,4));
} catch(e){
	console.log(e);
}


// mul
console.log(mul_once(5,5));
try{
	console.log(mul_once(5,5));
}catch(e){
	console.log(e);
}


// Aufgabe 12

function counterf(x){
	counter = new Object();
  
  counter.value = x;
  
  counter.inc = function() {
  	counter.value += 1;
  	return counter.value;
  };
  
  counter.dec = function() {
  	counter.value -= 1;
  	return counter.value;
  };
  
	return counter;
}

var c = counterf(10);

console.log(c.inc());
console.log(c.dec());
console.log(c.dec());

// Aufgabe 13

function revocable(func1){
	var ret = new Object();
  ret.revoked = false;
  
  ret.revoke = function() {
  	ret.revoked = true;
  };
  
	ret.invoke = function(x){
    if(!ret.revoked){
  		return func1(x);
    } else {
    	throw "revoked";
    }
  };
  return ret;
}

var r = revocable(alert);

r.invoke(7);
r.revoke();

try{
	r.invoke(8);
}catch(e){
	console.log(e);
}


// Aufgabe 14
function arrayWrapper(){
	var wrapper = new Object();
  
  var value = [];
  
  wrapper.get = function(index) { 
  	return value[index];
  };
  
  wrapper.store = function(index, val) {
  	value[index] = val;
  };
  
  wrapper.append = function(val) {
    value.push(val);
    return val;
  }
  
 	return wrapper; 
}


var a = arrayWrapper();

a.store(5, "Auto");
a.append("Hase");

console.log(a.get(5));
console.log(a.get(6));