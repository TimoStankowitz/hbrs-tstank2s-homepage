/*
 * Diese Aufgaben wurden in Zusammenarbeit mit Tim van der Meulen bearbeitet (tmeule2s) 
 */

// queue

function createQueue(){
	var values = [];
  var queue = new Object();
  queue.add = function(value){
  	values.push(value);
  }
  queue.isEmpty = function(){
  	return values.length == 0;
  }
  queue.take = function(){
  	if(queue.isEmpty()) throw "empty";
  	return values.shift();
  }
 	return queue;
}

var q = createQueue();

q.add(2);
q.add(3);
q.add(5);
console.log(q.take());
q.add(6);
console.log(q.take());
console.log(q.take());
console.log(q.take());
console.log(q.isEmpty());
console.log(q.take());




// set

function createSet(){
	var values = [];
  var set = new Object();
  set.add = function(value){
  	values[value] = value;
  }
  set.isEmpty = function(){
  	return values.length == 0;
  }
  set.remove = function(value){
  	if(set.isEmpty()) throw "empty";
  	values = values.filter(item => item != value);
  }
  set.contains = function(value){
  	return values[value] != null;
  }
 	return set;
}

var s = createSet();

console.log(s.isEmpty());
console.log(s.contains(8));
s.add(5);
console.log(s.isEmpty());
console.log(s.contains(5));
s.remove(5);
console.log(s.contains(5));
console.log(s.isEmpty());

s.add(2);
s.add(3);
s.add(5);
console.log(s.contains(3));
console.log(s.contains(6));
s.add(6);
console.log(s.contains(6));
s.remove(5);
console.log(s.contains(5));
console.log(s.isEmpty());







// multi set

function createSet(){
	var values = [];
  var set = new Object();
  set.add = function(value){
  	values.push(value);
  }
  set.isEmpty = function(){
  	return values.length == 0;
  }
  var firstIndexOf = function(value){
  	var counter = 0;
    while(counter < values.length){
    	if(values[counter] == value)return counter;
      counter++;
    }
    return -1;
  }
  set.remove = function(value){
    var index = firstIndexOf(value);
    if(index == -1) throw "not found";
  	values = values.slice(0, index).concat(values.slice(index + 1, values.length));
  }
  set.contains = function(value){
  	return firstIndexOf(value) != -1;
  }
 	return set;
}

var s = createSet();

console.log(s.isEmpty());
console.log(s.contains(8));
s.add(5);
console.log(s.isEmpty());
console.log(s.contains(5));
s.remove(5);
console.log(s.contains(5));
console.log(s.isEmpty());

console.log("------------------------");

s.add(2);
s.add(3);
s.add(5);
console.log(s.contains(3));
console.log(s.contains(6));
s.add(6);
console.log(s.contains(6));
s.remove(5);
console.log(s.contains(5));
console.log(s.isEmpty());
