/*
 * Diese Aufgaben wurden in Zusammenarbeit mit Tim van der Meulen bearbeitet (tmeule2s) 
 */

// Aufgabe 1

function pubsub() {
	var pubsub = new Object();
  
  var subscriber = [];
  
  pubsub.subscribe = function(subscriberName) {
  	subscriber.push(subscriberName);
  }
  
  pubsub.publish = function(message) {
		for (index = 0; index < subscriber.length; ++index) {
    	alert(message);
		}
  }

	return pubsub;
}


var my_pubsub = pubsub();
my_pubsub.subscribe(alert);
my_pubsub.publish("It works!")


// Aufgabe 2
function gensymf(letter) {
	counter = 0;
  
  return function() {
		console.log(letter + counter++)
  }
  
}

var gensym = gensymf('G');
gensym();
gensym();
gensym();
gensym();

// Aufgabe 3 (nicht ganz richtig)
function fibonaccif(no1, no2) {
  counter = 0;
  fibNumberStart = 0;
  
  return function fub() {  
  	if(counter == 0) {
    	counter++;
    	fibNumberStart = (no1 + no2);
    	return fibNumberStart;
  	}
  
  	var a = 1, b = 0, temp;

  	while (fibNumberStart >= 0){
    	temp = a;
    	a = a + b;
    	b = temp;
    	fibNumberStart--;
  	}

		fibNumberStart = b+1;
  	return b;
  }
}


var fib = fibonaccif(0, 1);
console.log(fib());
console.log(fib());
console.log(fib());
console.log(fib());
console.log(fib());
console.log(fib());



// Aufgabe 4 - addg



// Aufgabe 5 - applyg

// Aufgabe 6 - JSON m
function m(value, source) {
  return obj = {
    value: value,
    source: (source == null) ? value : source
  }
}


out1 = JSON.stringify(m(1));
out2 = JSON.stringify(m(Math.PI, "pi"));

console.log(out1);
console.log(out2);



// Aufgabe 7 - addm

function m(value, source) {
  return obj = {
    value: value,
    source: (source == null) ? value : source
  }
}

function addm(m1, m2) {
  return obj = {
    value: m1.value + m2.value,
    source: "(" + m1.value + "+" + m2.value + ")"
  }
}

out = JSON.stringify(addm(m(3), m(4)));

console.log(out);


// Aufgabe 8 - binarymf

function add(x, y) {
	return x + y;
}

function mul(x, y) {
	return x * y;
}

function m(value, source) {
  return obj = {
    value: value,
    source: (source == null) ? value : source
  }
}


function binarymf(operation, operationSymbol) {
  return function(m1, m2) {
    return obj = {
      value: operation(m1.value, m2.value),
      source: "(" + m1.value + operationSymbol + m2.value + ")"
    }
  }
}

addm = binarymf(add, "+");
mulm = binarymf(mul, "*");

out1 = JSON.stringify(addm(m(3), m(4)));
out2 = JSON.stringify(mulm(m(5), m(5)));

console.log(out1);
console.log(out2);


// Aufgabe 9  - binarymf 2
function add(x, y) {
	return x + y;
}

function mul(x, y) {
	return x * y;
}

function m(value, source) {
  return obj = {
    value: value,
    source: (source == null) ? value : source
  }
}


function binarymf(operation, operationSymbol) {
  return function(arg1, arg2) {
    if(typeof(arg1) == "object") {
      no1 = arg1.value
    } else {
      no1 = arg1;
    }

    if(typeof(arg2) == "object") {
      no2 = arg2.value
    } else {
      no2 = arg2;
    }

    return obj = {
      value: operation(no1, no2),
      source: "(" + no1 + operationSymbol + no2 + ")"
    }
  }
}

addm = binarymf(add, "+");

out1 = JSON.stringify(addm(m(3), m(4)));
out2 = JSON.stringify(addm(3, 4));

console.log(out1);
console.log(out2);


// Aufgabe 10 - unarymf
function square(number) {
  return number * number;
}

function unarymf(operation, operationName) {
  return function(number) {
    return obj = {
      value: operation(number),
      source: "(" + operationName + " " + number + ")"
    }
  }
}

squarem = unarymf(square, "square");

out = JSON.stringify(squarem(4))

console.log(out)

// Aufgabe 11 - hypotenuse
function hyp(x, y) {
  return Math.sqrt(x*x + y*y)
}

out = hyp(3,4)

console.log(out);


// Aufgabe 12 - Komische Mathe Aufgabe
// Wurde nicht gelößt, da zu wenig Zeit.

// Aufgabe 13 - Store
function store(value) {
  variable = value;
  return value;
}

var variable; 
store(5);

console.log(variable);

// Aufgabe 14 - quatre
function store(value) {
  variable = value;
  return value;
}


function identityf(value) {
  return value;
}

function quatre(binF, op1, op2, res) {
  res(binF(op1, op2))
}

var variable; 
quatre(add, identityf(3), identityf(4), store); 

console.log(variable);

// Aufgabe 15 - unaryc
// TODO

// Aufgabe 14
function add(x, y) {
	return x + y;
}

function mul(x, y) {
	return x * y;
}

function store(value) {
  variable = value;
  return value;
}

function binaryc(operationFunction) {
  return function(no1, no2, storefunction) {
    storefunction(operationFunction(no1, no2))
  }
}

addc = binaryc(add);
addc(4, 5, store);

mulc = binaryc(mul);
mulc(2,3, store); 

console.log(variable);




