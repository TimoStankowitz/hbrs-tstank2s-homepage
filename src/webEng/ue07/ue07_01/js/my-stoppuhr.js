import { LitElement, html, css } from 'https://unpkg.com/lit-element/lit-element.js?module';

class MyStoppuhr extends LitElement {

  static get properties() {
    return {
      isOn:         { type: Boolean },
      displayTime:  { type: String },
      time:         { type: Number },
      interval:     { type: Number },
      offset:       { type: Number }
    }
  }

  constructor() {
    super();
    
    this.isOn = false;
    this.time = 0;
    this.displayTime = "00 : 00 . 000";

  }

  static get styles() {
    return css`
      button {
        background: lightblue;
        color: green;
        border: 1px solid black;
        border-radius: 3px;
        width: 100px;
        height: 35px;
        transition: 0.3s;
      }

      button:hover {
        cursor: pointer;
        background: blue;
        color: #ffffff;
      }
    `;
  }

  watchUpdate() {
    if(this.isOn) {
      this.time += this.watchDelta();
    }

    var formattedTime = this.watchTimeFormatter(this.time);
    this.displayTime = formattedTime;
  }

  watchDelta() {
    var now = Date.now();
    var timePassed = now - this.offset;
    this.offset = now;
    return timePassed;
  }

  watchTimeFormatter(timeInMilliseconds) {
    var time = new Date(timeInMilliseconds);
    var minutes = time.getMinutes().toString();
    var seconds = time.getSeconds().toString();
    var milliseconds = time.getMilliseconds().toString();

    if(minutes.length < 2) {
      minutes = '0' + minutes;

    }

    if(seconds.length < 2) {
      seconds = '0' + seconds;
    }

    while (milliseconds.length < 3) {
      milliseconds = '0' +milliseconds;
    }

    return minutes + ' : ' + seconds + ' . ' + milliseconds;
  }

  watchStart() {
    if(!this.isOn) {
      this.interval = setInterval(this.watchUpdate.bind(this), 10);
      this.offset = Date.now();
      this.isOn = true;
    }
  }

  watchStop() {
    if(this.isOn) {
      clearInterval(this.interval);
      this.interval = null;
      this.isOn = false;
    }
  }

  watchReset() {
    if(!this.isOn) {
      this.time = 0;
      this.watchUpdate();
    }
  }

  start() {
    this.watchStart();
  }

  stop() {
    this.watchStop();
  }

  clickStart() {
    this.isOn ? this.stop() : this.start();
  }

  clickReset() {
    this.watchReset();
  }

  render() {
    return html`
      <p>${this.displayTime}</p>
      <button @click="${this.clickStart}">${this.isOn ? html`stop` : html`start`}</button>
      <button @click="${this.clickReset}">reset</button>
    `;
  }
}

customElements.define('my-stoppuhr', MyStoppuhr);

